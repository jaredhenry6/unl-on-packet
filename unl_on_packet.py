'''
So Simple UNL Setup On Packet.net
This will setup UNL on bare metal on Packet. 
This will also copy over the necessary images using paramiko.
jhenry6@gmail.com

There is no Deadman Switch. So It will not automatically kill the
instance. You will have to do this on your own.
'''

import packet
import prowlpy
import time
import paramiko
import sys
import urllib
from scpclient import Write, closing


def main():
    prowl_messenger = prowlpy.Prowl(prowl_key)
    manager = packet.Manager(auth_token=authorization_token)
    device = manager.create_device(project_id=project_identity,
                                   hostname='UNL',
                                   plan=server_size, facility=datacenter_facilty,
                                   operating_system='ubuntu_14_04',
                                   userdata='#!/bin/sh\ncurl -s http://www.unetlab.com/install.sh | bash\ntouch /opt/ovf/.configured\n')
    devices = manager.list_devices(project_identity)
    for x in devices:
        my_ip = x.ip_addresses[0]['address']
        state = x.state
    while True:
        time.sleep(30)
        devices = manager.list_devices(project_identity)
        for x in devices:
            state = x.state
        if state == 'active':
            time.sleep(300)
            break
        else:
            pass
    
    #SCP my Files over and then setup the firewall to only allow the external IP
    #address of the one setting up the lab.
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=my_ip, username='root', key_filename=key_ssh_pub)
    with closing(Write(ssh_client.get_transport(), '.')) as scp:
        scp.send_file('vIOS-L3.qcow2')
    with closing(Write(ssh_client.get_transport(), '.')) as scp:
        scp.send_file('c7200-adventerprisek9-mz.152-4.S6.image')
    with closing(Write(ssh_client.get_transport(), '.')) as scp:
        scp.send_file('c7200.php')
    stdin,stdout,stderr = ssh_client.exec_command('mv c7200-adventerprisek9-mz.152-4.S6.image /opt/unetlab/addons/dynamips/')
    stdin,stdout,stderr = ssh_client.exec_command('mkdir -p /opt/unetlab/addons/qemu/vios-adventerprisek9-m-15.4-1.3.0-181')
    stdin,stdout,stderr = ssh_client.exec_command('mv vIOS-L3.qcow2 /opt/unetlab/addons/qemu/vios-adventerprisek9-m-15.4-1.3.0-181/hda.qcow2')
    stdin,stdout,stderr = ssh_client.exec_command('/opt/unetlab/wrappers/unl_wrapper -a fixpermissions')
    stdin,stdout,stderr = ssh_client.exec_command('ufw allow ssh')
    stdin,stdout,stderr = ssh_client.exec_command('ufw allow from ' + my_external_ip)
    stdin,stdout,stderr = ssh_client.exec_command('rm /opt/unetlab/html/templates/c7200.php && mv c7200.php /opt/unetlab/html/templates/')
    stdin,stdout,stderr = ssh_client.exec_command('ufw --force enable')
    prowl_messenger.add('UNL Lab Is Ready','Username:admin Password:unl - Please change it on first login.',my_ip,1,None,None)
    
    '''
    devices = manager.list_devices(project_identity)
    for x in devices:
        if 'UNL' in x.hostname:
            unl_dev_id = x.id
    '''

if __name__ == '__main__':
    #You can add this in your Packet.net account. You will need to add and API key via the
    #page on your account. This is your authorization token. 
    # https://app.packet.net/portal#/api-keys
    authorization_token = 'api_key_here'
    
    # This is the project Identity. You can derive this from the URL. 
    # I may rewrite this later to create a UNL project. Not teday.
    # https://app.packet.net/portal#/project/THIS_IS_THE_PROJECT_IDENTITY/servers
    project_identity = 'your_project_id'
    
    #You will need to find this one. ewr1 is the NJersey facility. 
    datacenter_facilty = 'ewr1' #Parsippany, NJ
    
    #Choose the Server Size:
    # baremetal_0
    # baremetal_1
    # baremetal_3
    # https://www.packet.net/bare-metal/
    server_size = 'baremetal_0'
    
    #Prowl API Key
    #This is how I am notifying myself that the server is ready to login. It is just pushing
    #/prowling me a message that the server is ready.
    # https://www.prowlapp.com/
    prowl_key = 'prowl_api_key'
    
    #This is the key that you generated on your PC. You will need to go in your Packet.net
    #and add this key. You will then need to set your key as below.
    # https://app.packet.net/portal#/ssh-keys
    # https://www.packet.net/resources/kb/how-do-i-generate-ssh-keys/
    key_ssh_pub = '/Users/henryjc/.ssh/id_rsa.pub'

    #Get my external IP address to setup UFW firewall to only allow me to telnet to my devices.
    my_external_ip = urllib.urlopen('http://icanhazip.com/').read().split('\n')[0]
    main()






