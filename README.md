#UNL on Packet
This is a simple python script that will install Unified Networking Lab on a
packet.net bare metal server. This is very simple and allows you to do a
lot without having to purchase a full blown server. 

This is really awesome for:
-CCIE Labs
-CCNA Labs
-CCNP Labs
-Networking Labs
0r whatever else. 

Look at the code for detailed information what to do. 

You will need to provide your own images but you can see how I am adding them
to the server once it is setup. It is a Q&D paramiko scp the files over. I will
maybe eventually setup a cleaner way. 

Jared
